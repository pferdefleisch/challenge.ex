defmodule Challenge.LinkController do
  use Challenge.Web, :controller
  alias Challenge.PageServer
  require Logger
  require IEx

  def show(conn, %{"link" => link}) do
    case PageServer.page(link) do
      :error -> 
        html conn, "Not cached"
      {:ok, %{html: html, url: url}} ->
        log_request(conn, url)
        html conn, html
    end
  end

  defp log_request(conn, url) do
    %{"req_headers": headers, "remote_ip": ip} = conn
    h = Enum.into(headers, %{})
    referer = h["referer"]
    user_agent = h["user-agent"]
    remote_ip = Tuple.to_list(ip) |> Enum.join(".")
    log_message = %{remote_ip: remote_ip, original_url: url, referer: referer, user_agent: user_agent, at: :calendar.local_time()}
    Logger.info(inspect log_message)
  end
end

