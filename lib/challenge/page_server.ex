defmodule Challenge.PageServer do
  use GenServer

  # Public API

  def start_link do
    GenServer.start_link(__MODULE__, %{}, name: :page_repo)
  end

  def page(link) do
    GenServer.call(:page_repo, {:lookup, link})
  end

  def save(page) do
    GenServer.cast(:page_repo, {:update, page})
  end

  # Private API
  
  # state looks like:
  # %{short_link: url:, html:}
  #
  def handle_call({:lookup, short_link}, _from, pages) do
    {:reply, Map.fetch(pages, short_link), pages}
  end

  def handle_cast({:update, %{short_link: link, url: url, html: html}}, pages) do
    pages = Map.put(pages, link, %{url: url, html: html})
    {:noreply, pages}
  end
end
